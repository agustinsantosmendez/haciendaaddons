package es.gob.hacienda.bitemporal;

import es.gob.hacienda.bitemporal.entity.BitemporalPeriods;
import es.gob.hacienda.bitemporal.entity.EditionStatus;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Interface for Bitemporal entities.
 */
public interface BitemporalEntity {

    EditionStatus getStatus();

    void setStatus(EditionStatus status);

    String getEditVersion();

    void setEditVersion(String editVersion);

    LocalDateTime getVt_End();

    void setVt_End(LocalDateTime vt_End);

    LocalDateTime getVt_Start();

    void setVt_Start(LocalDateTime vt_Start);

    LocalDateTime getTt_Start();

    void setTt_Start(LocalDateTime tt_Start);

    void setTt_End(LocalDateTime tt_End);

    LocalDateTime getTt_End();
}
