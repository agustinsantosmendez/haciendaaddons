package es.gob.hacienda.bitemporal.entity;

import es.gob.hacienda.bitemporal.BitemporalEntity;
import io.jmix.core.entity.annotation.EmbeddedParameters;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class BaseBitemporalEntity implements BitemporalEntity {

    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    @EmbeddedParameters(nullAllowed = false)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "editVersion", column = @Column(name = "BITEMPORAL_EDIT_VERSION", nullable = false, length = 15)),
            @AttributeOverride(name = "status", column = @Column(name = "BITEMPORAL_STATUS", nullable = false)),
            @AttributeOverride(name = "vt_Start", column = @Column(name = "BITEMPORAL_VT_START", nullable = false)),
            @AttributeOverride(name = "vt_End", column = @Column(name = "BITEMPORAL_VT_END", nullable = false)),
            @AttributeOverride(name = "tt_Start", column = @Column(name = "BITEMPORAL_TT_START", nullable = false)),
            @AttributeOverride(name = "tt_End", column = @Column(name = "BITEMPORAL_TT_END", nullable = false))
    })
    private BitemporalPeriods bitemporal;

    public BitemporalPeriods getBitemporal() {
        return bitemporal;
    }

    public void setBitemporal(BitemporalPeriods bitemporal) {
        this.bitemporal = bitemporal;
    }

    @Override
    public EditionStatus getStatus() {
        return this.bitemporal.getStatus();
    }

    @Override
    public void setStatus(EditionStatus status) {
        this.bitemporal.setStatus(status);
    }

    @Override
    public String getEditVersion() {
        return this.bitemporal.getEditVersion();
    }

    @Override
    public void setEditVersion(String editVersion) {
        this.bitemporal.setEditVersion(editVersion);
    }

    @Override
    public LocalDateTime getVt_Start() {
        return this.bitemporal.getVt_Start();
    }

    @Override
    public void setVt_Start(LocalDateTime vt_Start) {
        this.bitemporal.setVt_Start(vt_Start);
    }

    @Override
    public LocalDateTime getVt_End() {
        return this.bitemporal.getVt_End();
    }

    @Override
    public void setVt_End(LocalDateTime vt_End) {
        this.bitemporal.setVt_End(vt_End);
    }


    @Override
    public LocalDateTime getTt_Start() {
        return this.bitemporal.getTt_Start();
    }

    @Override
    public void setTt_Start(LocalDateTime tt_Start) {
        this.bitemporal.setTt_Start(tt_Start);
    }

    @Override
    public LocalDateTime getTt_End() {
        return this.bitemporal.getTt_End();
    }

    @Override
    public void setTt_End(LocalDateTime tt_End) {
        this.bitemporal.setTt_End(tt_End);
    }
}
