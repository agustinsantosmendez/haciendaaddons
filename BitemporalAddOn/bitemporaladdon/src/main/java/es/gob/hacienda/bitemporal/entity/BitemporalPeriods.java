package es.gob.hacienda.bitemporal.entity;

import es.gob.hacienda.bitemporal.Version;
import io.jmix.core.Messages;
import io.jmix.core.metamodel.annotation.DependsOnProperties;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.Objects;

@JmixEntity(name = "BITEMP_BitemporalPeriods")
@Embeddable
public class BitemporalPeriods {
    public static final LocalDateTime INFINITE = LocalDateTime.of(2999, 12, 31, 23, 59, 59);

    @Pattern(regexp = "^([a-zA-Z]{1,5})?\\d{1,5}\\.\\d{1,6}(\\.\\d{1,6})?$")
    @NotBlank(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.editVersion.validation.NotEmptyOrBlank}")
    @NotEmpty(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.editVersion.validation.NotEmptyOrBlank}")
    @Column(name = "EDIT_VERSION", nullable = false, length = 24)
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    private String editVersion;

    @Column(name = "STATUS", nullable = false)
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    private String status;

    @Column(name = "VT_START", nullable = false)
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    private LocalDateTime vt_Start;

    @Column(name = "VT_END", nullable = false)
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    private LocalDateTime vt_End;

    @Column(name = "TT_START", nullable = false)
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    private LocalDateTime tt_Start;

    @FutureOrPresent(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.FutureOrPresent}")
    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    @Column(name = "TT_END", nullable = false)
    private LocalDateTime tt_End;

    public boolean isVt_Infinite() {
        return vt_End.equals(INFINITE);
    }
    public void setVt_Infinite() {
        this.vt_End = INFINITE;
    }

    public boolean isTt_Infinite() {
        return tt_End.equals(INFINITE);
    }
    public void setTt_Infinite() { this.tt_End = INFINITE;  }


    public EditionStatus getStatus() {
        return status == null ? null : EditionStatus.fromId(status);
    }

    public void setStatus(EditionStatus status) {
        this.status = status == null ? null : status.getId();
    }

    public String getEditVersion() {
        return editVersion;
    }

    public void setEditVersion(String editVersion) {
        this.editVersion = editVersion;
    }

    public LocalDateTime getVt_End() {
        return vt_End;
    }

    public void setVt_End(LocalDateTime vt_End) {
        this.vt_End = vt_End;
    }

    public LocalDateTime getVt_Start() {
        return vt_Start;
    }

    public void setVt_Start(LocalDateTime vt_Start) {
        this.vt_Start = vt_Start;
    }

    public LocalDateTime getTt_Start() {
        return tt_Start;
    }

    public void setTt_Start(LocalDateTime tt_Start) {
        this.tt_Start = tt_Start;
    }

    public void setTt_End(LocalDateTime tt_End) {
        this.tt_End = tt_End;
    }

    public LocalDateTime getTt_End() {
        return tt_End;
    }

    public void setValidTime(LocalDateTime start, LocalDateTime end) {
        this.setVt_Start(start);
        this.setVt_End(end);
    }

    public void setTransactionTime(LocalDateTime start, LocalDateTime end) {
        this.setTt_Start(start);
        this.setTt_End(end);
    }

    public static void copyRegs(BitemporalPeriods from, BitemporalPeriods to) {
        to.setStatus(from.getStatus());
        to.setEditVersion(from.getEditVersion());
        to.setVt_Start(from.getVt_Start());
        to.setVt_End(from.getVt_End());
        to.setTt_Start(from.getTt_Start());
        to.setTt_End(from.getTt_End());
    }
    private void doMajorChange(BitemporalPeriods oldReg, BitemporalPeriods newReg) {
        copyRegs(oldReg, newReg);
        newReg.setStatus(EditionStatus.OPEN_MAJOR);
        Version ver = new Version(oldReg.getEditVersion());
        newReg.setEditVersion(ver.getIncrementedMajorVersion());
    }

    private void doMinorChange(BitemporalPeriods oldReg, BitemporalPeriods newReg) {
        copyRegs(oldReg, newReg);
        newReg.setStatus(EditionStatus.OPEN_MINOR);
        Version ver = new Version(oldReg.getEditVersion());
        newReg.setEditVersion(ver.getIncrementedMinorVersion());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof BitemporalPeriods)) {
            return false;
        }
        BitemporalPeriods abstractValueObject = (BitemporalPeriods) o;
        return Objects.equals(editVersion, abstractValueObject.editVersion) &&
                Objects.equals(vt_Start, abstractValueObject.vt_Start) &&
                Objects.equals(vt_End, abstractValueObject.vt_End);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.editVersion, this.vt_Start, this.vt_End);
    }

    @Override
    public String toString() {
        return "BitemporalPeriod [ version=" + this.editVersion + ", from=" + this.vt_Start + ", until=" + this.vt_End + "]";
    }

    @InstanceName
    @DependsOnProperties({"editVersion", "vt_Start", "vt_End"})
    public String getDisplayName(Messages messages) {
        return messages.formatMessage(
                getClass(), "BitemporalPeriods.instanceName",
                this.editVersion, this.vt_Start, this.vt_End);
    }
}