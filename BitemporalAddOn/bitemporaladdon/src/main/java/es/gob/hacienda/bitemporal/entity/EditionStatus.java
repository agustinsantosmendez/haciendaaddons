package es.gob.hacienda.bitemporal.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum EditionStatus implements EnumClass<String> {

    OPEN_MAJOR("Major edition"),
    OPEN_MINOR("Minor edition"),
    VALID("Valid"),
    NOT_VALID("No Valid"),
    DELETED("Deleted");

    private String id;

    EditionStatus(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static EditionStatus fromId(String id) {
        for (EditionStatus at : EditionStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}