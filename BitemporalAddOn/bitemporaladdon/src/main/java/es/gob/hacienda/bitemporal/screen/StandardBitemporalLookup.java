package es.gob.hacienda.bitemporal.screen;

import io.jmix.ui.screen.LookupScreen;
import io.jmix.ui.screen.MultiSelectLookupScreen;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.StandardLookup;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Base class for lookup screens with bitemporalv periods.
 *
 * @param <T> type of entity
 */
public class StandardBitemporalLookup<T> extends StandardLookup<T> {

}
