package es.gob.hacienda.bitemporal.screen;

import es.gob.hacienda.bitemporal.BitemporalEntity;
import es.gob.hacienda.bitemporal.entity.BitemporalPeriods;
import es.gob.hacienda.bitemporal.entity.EditionStatus;
import io.jmix.core.DataManager;
import io.jmix.ui.Dialogs;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.app.inputdialog.DialogActions;
import io.jmix.ui.app.inputdialog.DialogOutcome;
import io.jmix.ui.app.inputdialog.InputParameter;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.StandardEditor;
import io.jmix.ui.screen.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.Optional;

public class StandardBitermporalEditor<T extends BitemporalEntity> extends StandardEditor<T> {
    @Autowired
    private DataContext dataContext;

    @Autowired
    private DataManager dataManager;

    @Autowired
    private Dialogs dialogs;

    public void getEffectiveDates(T sec) {
        if (sec.getStatus() == EditionStatus.OPEN_MAJOR) {
            dialogs.createInputDialog(this)
                    .withCaption("Obtener Fechas de Validez")
                    .withParameters(
                            InputParameter.localDateTimeParameter("beginInstant")
                                    .withCaption("Fecha inicio")
                                    .withRequired(true),
                            InputParameter.booleanParameter("hasEndInstant")
                                    .withCaption("¿Existe fecha final?")
                                    .withRequired(true),
                            InputParameter.localDateTimeParameter("endInstant")
                                    .withCaption("Fecha final")
                                    .withRequired(false)
                    )
                    .withActions(DialogActions.OK_CANCEL)
                    .withCloseListener(closeEvent -> {
                        if (closeEvent.closedWith(DialogOutcome.OK)) {
                            LocalDateTime from = closeEvent.getValue("beginInstant");
                            Boolean hasEnd = closeEvent.getValue("hasEndInstant");
                            LocalDateTime to;
                            if (hasEnd)
                                to = closeEvent.getValue("endInstant");
                            else
                                to = BitemporalPeriods.INFINITE;
                            closeMajor(sec, from, hasEnd, to);
                            closeWithCommit();
                        } else
                            closeWithDiscard();
                    })
                    .show();
        } else if (sec.getStatus() == EditionStatus.OPEN_MINOR) {
            dialogs.createOptionDialog()
                    .withCaption("Por favor, confirme")
                    .withMessage("¿Desea cerrar la versión?")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES, Action.Status.PRIMARY)
                                    .withHandler(e ->
                                    {
                                        closeMinor(sec);
                                        closeWithCommit();
                                    }),
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .show();
        }
    }
    private  void closeMajor(T sec, LocalDateTime from, Boolean hasEnd, LocalDateTime to) {
 /*
        Optional<T> regopt = dataManager.load(T.class)
                .query("e.name = ?1 and e.status = 'Valid' order by e.effectivePeriod.from desc", sec.getName())
                .optional();

        // is there a previous version?
        if (regopt.isPresent()) {
            T reg2 = regopt.get();
            EffectivePeriod tmp = reg2.getEffectivePeriod();
            reg2.setEffectivePeriod(tmp.getFrom(), from);
            reg2.setStatus(EditionStatus.NOT_VALID);
            dataManager.save(reg2);
        }
        sec.setEffectivePeriod(from, to);
        sec.setRecordPeriod(LocalDateTime.now(), RecordPeriod.INFINITE);
        sec.setStatus(EditionStatus.VALID);
 */
    }

    private  void closeMinor(T sec) {
 /*
        Optional<T> regopt = dataManager.load(T.class)
                .query("e.name = ?1 and e.status = 'Valid' order by e.effectivePeriod.from desc", sec.getName())
                .optional();

        // is there a previous version?
        if (regopt.isPresent()) {
            T reg2 = regopt.get();
            reg2.setStatus(EditionStatus.NOT_VALID);
            RecordPeriod tmp = reg2.getRecordPeriod();
            reg2.setRecordPeriod(tmp.getFrom(), LocalDateTime.now());
            dataManager.save(reg2);

        }

        sec.setRecordPeriod(LocalDateTime.now(), RecordPeriod.INFINITE);
        sec.setStatus(EditionStatus.VALID);
  */
    }


    @Subscribe("windowCloseVersionAndClose")
    public void onWindowCloseVersionAndClose(Action.ActionPerformedEvent event) {
        T secundaryTable = dataContext.merge(getEditedEntity());
        getEffectiveDates(secundaryTable);
    }
}
