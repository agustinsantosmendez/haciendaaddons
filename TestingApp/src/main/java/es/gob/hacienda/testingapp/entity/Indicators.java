package es.gob.hacienda.testingapp.entity;

import es.gob.hacienda.bitemporal.BitemporalEntity;
import es.gob.hacienda.bitemporal.entity.BaseBitemporalEntity;
import es.gob.hacienda.bitemporal.entity.BitemporalPeriods;
import es.gob.hacienda.bitemporal.entity.EditionStatus;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.EmbeddedParameters;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "INDICATORS", indexes = {
        @Index(name = "IDX_INDICATORS_UNQ", columnList = "IND_ID, BITEMPORAL_VT_START, BITEMPORAL_VT_END, BITEMPORAL_TT_START", unique = true)
})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DTYPE", discriminatorType = DiscriminatorType.STRING)
public class Indicators implements BitemporalEntity {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "IND_ID", nullable = false, length = 10)
    @NotNull
    private String ind_id;

    @Column(name = "METRIC", nullable = false)
    @NotNull
    private String metric;

    @InstanceName
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @NotNull(message = "{msg://es.gob.hacienda.bitemporal.entity/BitemporalPeriods.validation.NotNull}")
    @EmbeddedParameters(nullAllowed = false)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "editVersion", column = @Column(name = "BITEMPORAL_EDIT_VERSION", nullable = false, length = 15)),
            @AttributeOverride(name = "status", column = @Column(name = "BITEMPORAL_STATUS", nullable = false)),
            @AttributeOverride(name = "vt_Start", column = @Column(name = "BITEMPORAL_VT_START", nullable = false)),
            @AttributeOverride(name = "vt_End", column = @Column(name = "BITEMPORAL_VT_END", nullable = false)),
            @AttributeOverride(name = "tt_Start", column = @Column(name = "BITEMPORAL_TT_START", nullable = false)),
            @AttributeOverride(name = "tt_End", column = @Column(name = "BITEMPORAL_TT_END", nullable = false))
    })
    private BitemporalPeriods bitemporal;

    public BitemporalPeriods getBitemporal() {
        return bitemporal;
    }

    public void setBitemporal(BitemporalPeriods bitemporal) {
        this.bitemporal = bitemporal;
    }

    public EditionStatus getStatus() {
        return this.bitemporal.getStatus();
    }

    public void setStatus(EditionStatus status) {
        this.bitemporal.setStatus(status);
    }

    public String getEditVersion() {
        return this.bitemporal.getEditVersion();
    }

    public void setEditVersion(String editVersion) {
        this.bitemporal.setEditVersion(editVersion);
    }

    public LocalDateTime getVt_Start() {
        return this.bitemporal.getVt_Start();
    }

    public void setVt_Start(LocalDateTime vt_Start) {
        this.bitemporal.setVt_Start(vt_Start);
    }

    public LocalDateTime getVt_End() {
        return this.bitemporal.getVt_End();
    }

    public void setVt_End(LocalDateTime vt_End) {
        this.bitemporal.setVt_End(vt_End);
    }

    public LocalDateTime getTt_Start() {
        return this.bitemporal.getTt_Start();
    }

    @Override
    public void setTt_Start(LocalDateTime tt_Start) {
        this.bitemporal.setTt_Start(tt_Start);
    }

    public LocalDateTime getTt_End() {
        return this.bitemporal.getTt_End();
    }

    public void setTt_End(LocalDateTime tt_End) {
        this.bitemporal.setTt_End(tt_End);
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getInd_id() {
        return ind_id;
    }

    public void setInd_id(String ind_id) {
        this.ind_id = ind_id;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}