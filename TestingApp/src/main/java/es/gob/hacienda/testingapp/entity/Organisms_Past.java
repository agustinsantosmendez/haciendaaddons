package es.gob.hacienda.testingapp.entity;

import es.gob.hacienda.bitemporal.entity.BitemporalPeriods;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.EmbeddedParameters;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "ORGANISMS_PAST", indexes = {
        @Index(name = "IDX_ORGANISMS__PAST_UNQ", columnList = "ORG_ID, BITEMPORAL_VT_START, BITEMPORAL_VT_END, BITEMPORAL_TT_START, BITEMPORAL_TT_END", unique = true)
})
@Entity
public class Organisms_Past {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "ORG_ID", nullable = false, length = 10)
    @NotNull
    private String org_id;

    @Column(name = "DIRECTOR")
    private String director;

    @Column(name = "PRG_ID")
    private String prg_id;

    @InstanceName
    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull(message = "{msg://es.gob.hacienda.testingapp.entity/Organisms.bitemporal.validation.NotNull}")
    @EmbeddedParameters(nullAllowed = false)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "editVersion", column = @Column(name = "BITEMPORAL_EDIT_VERSION", nullable = false, length = 15)),
            @AttributeOverride(name = "status", column = @Column(name = "BITEMPORAL_STATUS", nullable = false)),
            @AttributeOverride(name = "vt_Start", column = @Column(name = "BITEMPORAL_VT_START", nullable = false)),
            @AttributeOverride(name = "vt_End", column = @Column(name = "BITEMPORAL_VT_END", nullable = false)),
            @AttributeOverride(name = "tt_Start", column = @Column(name = "BITEMPORAL_TT_START", nullable = false)),
            @AttributeOverride(name = "tt_End", column = @Column(name = "BITEMPORAL_TT_END", nullable = false))
    })
    private BitemporalPeriods bitemporal;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public String getOrg_id() {
        return org_id;
    }

    public String getDirector() {
        return director;
    }

    public String getPrg_id() {
        return prg_id;
    }

    public String getDescription() {
        return description;
    }

    public BitemporalPeriods getBitemporal() {
        return bitemporal;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}