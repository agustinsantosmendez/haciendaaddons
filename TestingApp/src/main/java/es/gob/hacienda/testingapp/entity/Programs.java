package es.gob.hacienda.testingapp.entity;

import es.gob.hacienda.bitemporal.entity.BitemporalPeriods;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.EmbeddedParameters;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "PROGRAMS")
@Entity
public class Programs {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "PRG_ID", nullable = false, length = 10)
    @NotNull
    private String prg_id;

    @DecimalMin("0")
    @Column(name = "BUDGET", nullable = false)
    @NotNull
    private Double budget;

    @InstanceName
    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @EmbeddedParameters(nullAllowed = false)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "editVersion", column = @Column(name = "BITEMPORAL_EDIT_VERSION", nullable = false, length = 15)),
            @AttributeOverride(name = "status", column = @Column(name = "BITEMPORAL_STATUS", nullable = false)),
            @AttributeOverride(name = "vt_Start", column = @Column(name = "BITEMPORAL_VT_START", nullable = false)),
            @AttributeOverride(name = "vt_End", column = @Column(name = "BITEMPORAL_VT_END", nullable = false)),
            @AttributeOverride(name = "tt_Start", column = @Column(name = "BITEMPORAL_TT_START", nullable = false)),
            @AttributeOverride(name = "tt_End", column = @Column(name = "BITEMPORAL_TT_END", nullable = false))
    })
    private BitemporalPeriods bitemporal;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public BitemporalPeriods getBitemporal() {
        return bitemporal;
    }

    public void setBitemporal(BitemporalPeriods bitemporal) {
        this.bitemporal = bitemporal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getPrg_id() {
        return prg_id;
    }

    public void setPrg_id(String prg_id) {
        this.prg_id = prg_id;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}