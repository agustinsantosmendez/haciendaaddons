package es.gob.hacienda.testingapp.screen.indicators;

import es.gob.hacienda.bitemporal.screen.StandardBitemporalLookup;
import io.jmix.ui.screen.*;
import es.gob.hacienda.testingapp.entity.Indicators;

@UiController("Indicators.browse")
@UiDescriptor("indicators-browse.xml")
@LookupComponent("indicatorsesTable")
public class IndicatorsBrowse extends StandardBitemporalLookup<Indicators> {
}