package es.gob.hacienda.testingapp.screen.indicators;

import io.jmix.ui.screen.*;
import es.gob.hacienda.testingapp.entity.Indicators;

@UiController("Indicators.edit")
@UiDescriptor("indicators-edit.xml")
@EditedEntityContainer("indicatorsDc")
public class IndicatorsEdit extends StandardEditor<Indicators> {
}